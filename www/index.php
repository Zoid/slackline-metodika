<?php

include "./layout/head.php";

?>

    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
            <div class="container">
                <a class="navbar-brand" href="#">Slack.cz</a>
                <button class="navbar-toggler" type="button"
                        data-toggle="collapse"
                        data-target="#navbar"
                        aria-controls="navbar"
                        aria-expanded="false"
                        aria-label="Toggle navigation"
                >
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbar">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="index.php">Přehled</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle"
                               href="#"
                               id="pouzivani-hl"
                               role="button"
                               data-toggle="dropdown"
                               aria-haspopup="true"
                               aria-expanded="false"
                            >
                                Používání highline
                            </a>
                            <div class="dropdown-menu" aria-labelledby="pouzivani-hl">
                                <a class="dropdown-item" href="/priprava">Příprava na highline</a>
                                <a class="dropdown-item" href="/bezpecnost">Bezpečnost</a>
                                <a class="dropdown-item" href="/vybaveni">Vybavení</a>
                                <a class="dropdown-item" href="/etika">Etika</a>
                                <a class="dropdown-item" href="/co-delat-na-hl">Co (ne)dělat na highline</a>
                                <hr class="dropdown-divider" />
                                <a class="dropdown-item" href="/slovnik">Slovníček pojmů</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle"
                               href="#"
                               id="kotveni-hl"
                               role="button"
                               data-toggle="dropdown"
                               aria-haspopup="true"
                               aria-expanded="false"
                            >
                                Kotvení & Materiály
                            </a>
                            <div class="dropdown-menu" aria-labelledby="kotveni-hl">
                                <a class="dropdown-item" href="/materialy">Materiály</a>
                                <a class="dropdown-item" href="/vybava">Výbava</a>
                                <a class="dropdown-item" href="/uzly">Uzly</a>
                                <a class="dropdown-item" href="/kotveni">Kotvení</a>
                                <a class="dropdown-item" href="/prirodni-kotveni">Přírodní kotvení</a>
                                <a class="dropdown-item" href="/master-point">Master point</a>
                                <a class="dropdown-item" href="/a-frames" class="btn-link disabled">A-Frame</a>
                                <a class="dropdown-item" href="/kilonewtony">Kilonewtony</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle"
                               href="#"
                               id="napinani-hl"
                               role="button"
                               data-toggle="dropdown"
                               aria-haspopup="true"
                               aria-expanded="false"
                            >
                                Napínání
                            </a>
                            <div class="dropdown-menu" aria-labelledby="napinani-hl">
                                <a class="dropdown-item" href="/priprava-lajny">Příprava</a>
                                <a class="dropdown-item" href="/budovani-kotveni">Budování kotvení</a>
                                <a class="dropdown-item" href="/napinani">Napínání</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/isa">Záchrana</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/podekovani">Poděkování</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container-fluid page-bg">
            <div class="row">
                <section class="main content-wrapper">
                    <div class="container">
                        <div class="row flex-column-reverse flex-md-row">
                            <div class="col-md-12">
                                <?php
                                    if(isset($_GET['page'])) {
                                        $page = $_GET['page'];
                                    } else {
                                        $page =  $_SERVER['REQUEST_URI'] ?? "index";
                                        if($page === "index.php") {
                                            $page = "index";
										}
                                    }

                                    $loadingDirs = [
                                            __DIR__ . '/pages/',
                                            __DIR__ . '/pages/pouzivani-highline/',
                                            __DIR__ . '/pages/materialy-a-kotveni/',
                                            __DIR__ . '/pages/napinani-highline/',
                                            __DIR__ . '/pages/zachrana/',
                                    ];

                                    $template = __DIR__ . '/pages/index.html';

                                    foreach ($loadingDirs as $dir) {
                                        $template = file_exists("$dir$page.html") ? "$dir$page.html" : $template;
                                    }

                                    include $template;
                                ?>
                            </div>
                        </div>
                    </div>
                </section>


                <footer class="footer">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="align-items-md-center text-center row">
                                    <div class="col-md-2 p-3">
                                        <a href="https://ceska-asociace-slackline.cz/" target="_blank">
                                            <img src="/images/logo/logo_cas.png">
                                        </a>
                                    </div>
                                    <div class="col-md-3 p-3">
                                        <a href="https://www.slacklineinternational.org/" target="_blank">
                                            <img src="/images/logo/logo_isa.png">
                                        </a>
                                    </div>
                                    <div class="col-md-2 p-3">
                                        <a href="https://www.hownot2.org/" target="_blank">
                                            <img src="/images/logo/logo_howNot2.png">
                                        </a>
                                    </div>
                                    <div class="col-md-3 p-3">
                                        <a href="https://www.eqb.cz/" target="_blank">
                                            <img src="/images/logo/logo_eqb.png">
                                        </a>
                                    </div>

                                    <div class="col-md-2 p-3">
                                        <a href="https://www.balancecommunity.com/" target="_blank">
                                            <img src="/images/logo/bc.jpg">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 pt-4 px-5 small text-center" style="margin-top: -4px">
                                Informace na této stránce sdružují nejrozšířenější metody a “best practices” používané v highline komunitě.
                                Pamatuj, že highlining provozuješ na své vlastní riziko a měl bys proto kriticky přemýšlet o tom, co a proč děláš.
                                <br><br>
                                Netahej svou první highiline sám! Naše komunitě má spoustu zkušených lidí, kteří ti rádi pomůžou, pokud si s
                                něčím nebudeš vědět rady. <br>Podívej se na skupinu
                                <a href="https://www.facebook.com/groups/114927001903775" target="_blank">Lajny - Kdy a kde</a> nebo na
                                <a href="https://www.facebook.com/groups/194644290710331" target="_blank">Českou asociaci slackline</a>.
                                <br><br>
                                Chyby v textu či překlepy můžeš hlásit <a href="mailto:info@czech-slackline.cz">zde</a>.
                                <p class="text-center">
                                    Slack.cz &copy; 2021
                                </p>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function() {
                $(".help-text").mouseover(function() {
                    $(this).children("img").show();
                });
                $(".help-text").mouseout(function() {
                    $(this).children("img").hide();
                });


                $("#mobile-menu").click(function (e) {
                    e.preventDefault();
                    let closed = $(".sidebar").hasClass("d-none");
                    if(closed) {
                        $(".sidebar").removeClass("d-none");
                    } else {
                        $(".sidebar").addClass("d-none");
                    }
                })

            });
        </script>

    </body>
</html>
