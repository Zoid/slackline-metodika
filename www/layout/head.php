<!doctype html>
<html lang="cs">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, viewport-fit=cover, user-scalable=no">

	<title>Slack.cz | Highline</title>

	<link rel="stylesheet"
		  href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css"
		  integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We"
		  crossorigin="anonymous"
	/>

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"
          integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ=="
          crossorigin="anonymous"
          referrerpolicy="no-referrer"
    />

	<style>
		@import url('https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;1,100;1,300;1,400&display=swap');
	</style>

	<style>

        /* TEMPLATE */
        .cz {
            background-image: url("/images/flags/cz.png");
            background-size: contain;
            width: 27px;
            height: 28px;
            margin-right: 3px;
        }

        .en {
            background-image: url("/images/flags/uk.png");
            background-size: contain;
            width: 27px;
            height: 28px;
            margin-right: 3px;
        }


        .content-nav .img {
            height: 250px;
            background-size: cover;
            background-position: center top;
            border: 1px solid black;
        }

        .content-nav a {
            text-decoration: none;
            color: black;
        }


        .boxes h5, .boxes a {
            font-size: 25px;
            text-decoration: none;
            color: black;
        }

        .box-image-holder {
            height: 225px;
            background-size: cover;
            background-position: center top;
        }

		.page-bg {
			background-image: url("images/bg.jpg");
			background-color: #a4a4a4;
			background-position: left -100px;
			background-repeat: no-repeat;
            background-attachment: fixed;

		}

        .danger-div {
            border: 5px solid #f04f35;
        }

        .danger-div ul {
            list-style: none;
            padding: 0;
        }

        .danger-div ul li {
            margin-bottom: 5px;
        }

        .line {
            border-bottom: 6px solid #f04f35;
            border-right: 17px solid transparent;
            height: 0;
            width: calc(100% - 335px);
            float: left;
            margin-top: 4px;
            position: absolute;
        }

        .triangle {
            width: 0;
            height: 0;
            border-top: 105px solid #14a89e;
            border-left: 315px solid transparent;
            float: right;
            margin-right: 2px;
            margin-top: 3px;
            margin-bottom: 5px;
        }

		.content-wrapper {
			margin-top: 140px;
		}

		.content-wrapper > .container > .row > div,
		.footer > .container > .row > div {
			background-color: white;
			border-radius: 3px;
		}

		.footer {
			margin-top: 50px;
		}


		ol > li {
			padding-bottom: 10px;
		}

        span.help-text {
            position: relative;
            text-decoration: 1px black underline;
            text-decoration-style: dotted;
            cursor: pointer;
            display: inline;
        }

        span.help-text img {
            position: absolute;
            width: 100px;
            height: auto;
            border: 1px solid #0000005e;
            box-shadow: 1px 1px 9px #000000a6;
            left: 0;
            display: none;
            top: 30px;
            z-index: 10;
        }

        .footer img {
            width: auto;
            max-height: 100px;
            max-width: 100%;
        }
	</style>

</head>
